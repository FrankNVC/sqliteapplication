package Part1_LinkedList;

import java.util.Iterator;

/**
 *
 * @author Nguyen Viet Cuong - s3393346
 */
public class TestSLL {

    public static void main(String[] args) {
        SortedLinkedList<String> sll = new SortedLinkedList<String>();

        System.out.println(sll);
        sll.addToHead(new String("Department"));
        System.out.println(sll);
        sll.addToHead(new String("Academics"));
        System.out.println(sll);
        sll.addToTail(new String("Interest"));
        System.out.println(sll);
        sll.addToTail(new String("Author"));
        System.out.println(sll);

        sll.sort();
        System.out.println(sll);

        sll.addToHead(new String("Paper"));
        sll.sort();
        System.out.println(sll);

        sll.add(new String("Field"));
        System.out.println(sll);

        sll.remove("Paper");
        System.out.println(sll);

        sll.remove("Author");
        System.out.println(sll);

        sll.remove("Field");
        System.out.println(sll);

        sll.remove("Academics");
        System.out.println(sll);

        sll.remove("Interest");
        System.out.println(sll);

        sll.remove("Department");
        System.out.println(sll + "null");
    }
}
