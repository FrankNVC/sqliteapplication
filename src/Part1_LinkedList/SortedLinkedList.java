package Part1_LinkedList;

import java.util.Iterator;

/**
 *
 * @author Nguyen Viet Cuong - s3393346
 * @param <T extends Comparable>
 */
public class SortedLinkedList<T extends Comparable> implements SortedList<T> {

    private Node head;
    private Node tail = new Node(getFromTail());
    private int count = 0;

    public SortedLinkedList() {
        head = null;    // empty list to start with, head refers to null
    }

    @Override
    public void addToHead(T obj) {
        // add new head node, and make its next refer to the old head 
        head = new Node(obj, head);
        count++;
    }

    @Override
    public void addToTail(T obj) {
        // if the list is empty, addToHead is the same as addToTail
        if (head == null) {
            addToHead(obj);
        } else {
            Node cur = head;
            Node prev = null;
            // find the tail of the list by going through all the nodes until the
            // end; refer to getFromTail for an alternative way to loop to the tail
            // of the list without needing a previous reference
            // in some cases (like in removeFromTail) we absolutely need to keep
            // track of the previous node (Why?)
            while (cur != null) {
                prev = cur;
                cur = cur.next;
            }
            prev.next = new Node(obj);
            count++;
        }
    }

    @Override
    public T getFromHead() {
        // always need to check for an empty list
        if (head == null) {
            return null;
        } else {
            return head.obj;
        }
    }

    @Override
    public T getFromTail() {
        // always need to check for an empty list
        if (head == null) {
            return null;
        } else {
            Node cur = head;
            // loop to find the tail of the list
            while (cur.next != null) {
                cur = cur.next;
            }
            return cur.obj;
        }
    }

    @Override
    public Iterator getIterator() {
        return new SLLIterator();
    }

    @Override
    public String toString() {
        // use a StringBuffer to build a String representation of the list
        // using an iterator to go through the list
        StringBuilder buf = new StringBuilder();
        Iterator iter = this.getIterator();

        while (iter.hasNext()) {
            buf.append(iter.next().toString()).append("\n");
            // this condition is necessary to avoid putting an arrow after
            // the last element; could have an else part to put a null 
            // reference "--|" at the end if desired
            // or we could modify the StringBuffer after the loop to "fix"
            // the last arrow
        }

        return buf.toString();
    }

    public String[] getString() {
        String[] s = new String[count];
        Iterator iter = this.getIterator();
        int i = 0;
        while (iter.hasNext()) {
            s[i] = iter.next().toString();
            i++;
        }
        return s;
    }

    @Override
    public T removeFromHead() {
        // always need to check for an empty list
        if (head == null) {
            return null;
        } else {
            // keep a reference to the old head
            T ret = head.obj;
            Node old = head;
            // move the head to the next node
            head = head.next;
            // clean up the next reference in the old head before returning
            old.next = null;
            count--;
            return ret;
        }
    }

    @Override
    public T removeFromTail() {
        // always need to check for an empty list
        if (head == null) {
            return null;
        } else {
            Node cur = head;
            Node prev = null;
            // loop to the tail of the list
            // we need to keep a reference to the previous node to update
            // previous.next correctly
            while (cur.next != null) {
                prev = cur;
                cur = cur.next;
            }
            // what if the list becomes empty after we remove the node?
            // if there is only one node in the list, then prev will be null
            if (prev == null) {
                head = null;
            } else {
                prev.next = null;
            }
            count--;
            return cur.obj;
        }
    }

    @Override
    public void sort() {
        int i = 0;
        Node current = head;
        Node previous = null;

        while (i < count) {
            while (current.next != null) {
                if (current.obj.compareTo(current.next.obj) > 0) {
                    T temp = current.next.obj;
                    current.next.obj = current.obj;
                    current.obj = temp;
                }
                previous = current;
                current = current.next;
            }
            previous = null;
            current = head;
            i++;
        }
    }

    @Override
    public boolean add(T obj) {
        tail = new Node(getFromTail());
        Node newNode = new Node(obj);

        // When the list is initially empty
        if (head == null) {
            addToHead(obj);
            return true;
        } else {
            // When the element to be added is less than the first element in the list
            if (obj.compareTo(head.obj) < 0) {
                addToHead(obj);
                return true;
            }
            // When the element to be added is greater than every element in in list
            // and has to be added at end of the list
            if (obj.compareTo(tail.obj) > 0) {
                addToTail(obj);
                return true;
            }
            //When the element to be added lies between other elements in the list
            if (obj.compareTo(head.obj) >= 0 && obj.compareTo(tail.obj) <= 0) {
                Node current = head.next;
                Node previous = head;
                while (obj.compareTo(current.obj) >= 0) {
                    previous = current;
                    current = current.next;
                }
                previous.next = newNode;
                newNode.next = current;
                count++;
            }
            return true;
        }
    }

    @Override
    public boolean remove(T obj) {
        //get the tail node
        tail = new Node(getFromTail());

        // When the list is initially empty
        if (head == null) {
            //no element to remove
            return false;
        } else if (obj.compareTo(head.obj) == 0) {
            //remove from head
            head = head.next;
            count--;
            return true;
        } else {
            //if obj greater than head, run thou
            Node current = head;
            Node previous = null;
            while (obj.compareTo(current.obj) != 0) {
                previous = current;
                current = current.next;
            }
            if (previous != null) {
                previous.next = current.next;
            } else {
                head = null;
            }
            count--;
            return true;
        }
    }

    // non-static inner class for SinglyLinkedList nodes
    // because it's non-static, a node cannot be created outside of a list
    // so a node is aware of its outer class
    class Node {

        private T obj;
        private Node next;

        Node(T obj) {
            this.obj = obj;
            next = null;
        }

        Node(T obj, Node next) {
            this.obj = obj;
            this.next = next;
        }

        @Override
        public String toString() {
            return "[" + obj + "]";
        }
    }

    // non-static inner class for a SinglyLinkedList iterator
    // because it's non-static, an iterator cannot be created outside of a list
    // so an iterator is aware of its outer class (look at the use of head 
    // inside remove()
    class SLLIterator implements Iterator {

        Node current;
        Node previous;

        SLLIterator() {
            this.current = head;
            previous = null;
        }

        @Override
        public boolean hasNext() {
            if (current != null) {
                return true;
            } else {
                return false;
            }
        }

        @Override
        public Object next() {
            if (!hasNext()) {
                return null;
            } else {    // if there is a next element
                // move current to the next
                previous = current;
                current = current.next;
                // return the obj inside the current node before it was moved
                return previous.obj;
            }
        }

        @Override
        public void remove() {
            // if there is a node to remove
            if (current != null) {
                Node temp = current;
                // if there is a previous then make the reference skip over
                // the current node
                if (previous != null) {
                    previous.next = current.next;
                } else {
                    // if there is no previous, it means we must be removing 
                    // the head of the list
                    head = current.next;
                }
                // move the current to the next node
                current = current.next;
                // and clean up the next reference of the deleted node
                temp.next = null;
            }
        }
    }
}
