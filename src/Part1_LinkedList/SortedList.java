package Part1_LinkedList;

/**
 *
 * @author Nguyen Viet Cuong - s3393346
 * @param <T>
 */
public interface SortedList<T extends Comparable> extends List<T> {

    public boolean add(T obj);

    public boolean remove(T obj);

    public void sort();
}
