package Part2_SQLiteApp;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Nguyen Viet Cuong - s3393346
 */
public class Message extends MouseAdapter {

    private JFrame frame = new JFrame();
    private JPanel panel = new JPanel();
    private JLabel label = new JLabel();

    //print error no connection
    public void printConnection() {

        frame = new JFrame();
        frame.setSize(300, 100);
        frame.setTitle("No Connection");

        label.setText("Database not connected yet.");
        panel.add(label);

        frame.setLayout(new FlowLayout());
        frame.add(panel);

        Button btnOK = new Button("OK");
        btnOK.addMouseListener(this); //add listener
        frame.add(btnOK);

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setAlwaysOnTop(true);
    }

    //print error no result
    public void printSQL() {

        frame = new JFrame();
        frame.setSize(300, 100);
        frame.setTitle("Wrong SQL");

        label.setText("No result. Check your SQL.");
        panel.add(label);

        frame.setLayout(new FlowLayout());
        frame.add(panel);

        Button btnOK = new Button("OK");
        btnOK.addMouseListener(this); //add listener
        frame.add(btnOK);

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setAlwaysOnTop(true);
    }

    //no query in JList is selected
    public void printSelected() {

        frame = new JFrame();
        frame.setSize(300, 100);
        frame.setTitle("No selection");

        label.setText("You did not select any query.");
        panel.add(label);

        frame.setLayout(new FlowLayout());
        frame.add(panel);

        Button btnOK = new Button("OK");
        btnOK.addMouseListener(this); //add listener
        frame.add(btnOK);

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setAlwaysOnTop(true);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        //click ok, hide the dialog
        frame.setVisible(false);
    }
}
