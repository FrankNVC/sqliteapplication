package Part2_SQLiteApp;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 *
 * @author Denis Rinfret
 * @modified Nguyen Viet Cuong
 */
public class TheTableController extends MouseAdapter {

    //the table model
    private TheTableModel model;
    //the table view
    private TheTableView view;

    //constructor
    public TheTableController(TheTableModel model) {
        this.model = model;
    }

    //connect the view
    public void setView(TheTableView view) {
        this.view = view;
    }

    @Override
    //mouse listener
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == view.jbtExecuteSQL) {
            //execute 1 query
            model.update(view.getSQL());
        } else if (e.getSource() == view.tableList.getTableJList()) {
            if (e.getClickCount() == 2) {
                //double click on JList
                model.getColumnName((String) view.tableList.getTableJList().getSelectedValue());
            }
        } else if (e.getSource() == view.jbtRemove) {
            if (model.getDB() != null) {
                //remove selected query from JList
                int index = view.queryList.getQueryJList().getSelectedIndex();
                if (index != -1) {
                    //remove from view
                    view.queryList.removeQuery(index);
                    //remove from array list in model
                    model.removeQuery(index);
                } else {
                    Message error = new Message();
                    error.printSelected();
                    view.jlbSQLStatus.setText("Error. No query selected.");
                }
            } else {
                Message error = new Message();
                error.printConnection();
                view.jlbSQLStatus.setText("Error. No open connection.");
            }
        }
    }
}