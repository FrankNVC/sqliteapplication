package Part2_SQLiteApp;

import Part1_LinkedList.SortedLinkedList;
import java.io.File;
import java.util.ArrayList;
import java.util.Observable;
import javax.swing.table.DefaultTableModel;

/**
 * Layer on top of an SQLiteQueryExecutor to model an employee table
 *
 * @author Denis Rinfret
 * @modified Nguyen Viet Cuong - s3393346
 */
public class TheTableModel extends Observable {

    //the sql command executor
    private SQLiteQueryExecutor db;
    //table name
    private SortedLinkedList<String> tableNameList = new SortedLinkedList<String>();
    //column name
    private SortedLinkedList<String> columnNameList = new SortedLinkedList<String>();
    //executed query
    private ArrayList<String> executedQueryList = new ArrayList<String>();
    //sql error
    private boolean sqlError;

    public TheTableModel() {
    }

    //write the table name to sorted linked list
    public void setTableName(DefaultTableModel table) {
        tableNameList = new SortedLinkedList<String>();
        //string array to store the value
        String[] s = new String[table.getRowCount()];
        for (int i = 0; i < s.length; i++) {
            s[i] = (String) table.getValueAt(i, 0);
        }
        //write the value to the linked list
        for (int i = 0; i < s.length; i++) {
            tableNameList.addToHead(s[i]);
        }
        //sort the linked list
        tableNameList.sort();
    }

    //write the column name to sorted linked list
    public void setColumnName(DefaultTableModel table) {
        columnNameList = new SortedLinkedList<String>();
        //string array to store value
        String[] s = new String[table.getRowCount()];
        //get column name using getTitles() method
        s = db.getTitles();
        //write the value to linked list
        for (int i = 0; i < s.length; i++) {
            columnNameList.addToHead(s[i]);
        }
        //sort the linked list
        columnNameList.sort();
    }

    //accessor
    public SortedLinkedList<String> getTableName() {
        return (tableNameList);
    }

    //accessor
    public SortedLinkedList<String> getColumn() {
        return (columnNameList);
    }

    //accessor
    public ArrayList<String> getQuery() {
        return (executedQueryList);
    }

    //accessor
    public SQLiteQueryExecutor getDB() {
        return db;
    }

    //accessor
    public DefaultTableModel getTableModel() {
        return db.getTableModel();
    }

    //set the database when user open sqlite file
    public void setDB(String dbFileName) {
        db = new SQLiteQueryExecutor(new File(dbFileName));
        db.executeQuery("SELECT name FROM sqlite_master WHERE type = 'table'");
        setTableName(db.getTableModel());
        columnNameList = new SortedLinkedList<String>();
        sqlError = true;
        setChanged();
        notifyObservers(this);
    }

    //execute 1 query
    public void update(String sqlQuery) {
        //check the database connection
        if (db != null) {
            //check if sql is correct
            if (db.executeQuery(sqlQuery)) {
                executedQueryList.add(sqlQuery);
                sqlError = true;
            } else {
                //error message for sql query
                Message error = new Message();
                error.printSQL();
                sqlError = false;
            }
        } else {
            //error message for connection
            Message error = new Message();
            error.printConnection();
            sqlError = false;
        }
        setChanged();
        notifyObservers(this);
    }

    //method to run query to get column name of one table
    public void getColumnName(String tableName) {
        try {
            String sqlQuery = "SELECT * FROM " + tableName;
            db.executeQuery(sqlQuery);
            //write the data to sorted linked list
            setColumnName(db.getTableModel());
            //notify the column JList in the view
            setChanged();
            notifyObservers(this);
        } catch (Exception e) {
            Message error = new Message();
            error.printConnection();
        }
    }

    //get error status
    public boolean getSQLError() {
        return sqlError;
    }

    //reset the executed query ArrayList
    public void setQuery() {
        executedQueryList = new ArrayList<String>();
    }

    //add executed query to ArrayList
    public void addQuery(String query) {
        executedQueryList.add(query);
    }

    //remove selected query from ArrayList
    public void removeQuery(int index) {
        executedQueryList.remove(index);
    }

    //update the query JList in the view
    public void viewQuery() {
        setChanged();
        notifyObservers(this);
    }

    //excute each query in the query ArrayList
    public void executeQuery(int i) {
        if (db != null) {
            //execute the query at index i of ArrayList
            db.executeQuery(executedQueryList.get(i));
            setChanged();
            notifyObservers(this);
        } else {
            Message error = new Message();
            error.printConnection();
        }
    }

    //disconnect to the database
    public void disconnectDB() {
        db.disconnect();
        db = null;
        tableNameList = new SortedLinkedList<String>();
        columnNameList = new SortedLinkedList<String>();
        executedQueryList = new ArrayList<String>();
    }
}
