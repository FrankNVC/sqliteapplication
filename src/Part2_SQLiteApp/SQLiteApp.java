package Part2_SQLiteApp;

import javax.swing.JFrame;

/**
 *
 * @author Nguyen Viet Cuong - s3393346
 */
public class SQLiteApp {

    //creating and connecting the mvc
    TheTableModel model = new TheTableModel();
    TheTableView view = new TheTableView(model);
    TheTableController controller = new TheTableController(model);

    //constructor
    public SQLiteApp() {
        view.jbtExecuteSQL.addMouseListener(controller);
        view.tableList.getTableJList().addMouseListener(controller);
        view.jbtRemove.addMouseListener(controller);
        model.addObserver(view);
        controller.setView(view);
        view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        view.pack();
        view.setVisible(true);
        view.setLocationRelativeTo(null);
    }

    public static void main(String[] args) {
        //run the app
        SQLiteApp app = new SQLiteApp();
    }
}
