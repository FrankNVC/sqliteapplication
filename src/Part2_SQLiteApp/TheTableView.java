package Part2_SQLiteApp;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Observable;
import java.util.Observer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.filechooser.FileFilter;

import jsyntaxpane.DefaultSyntaxKit;
import org.jcp.xml.dsig.internal.dom.Utils;

/**
 *
 * @author Nguyen Viet Cuong - s3393346
 */
public class TheTableView extends JFrame implements Observer, ActionListener {

    private TheTableModel model = new TheTableModel();
    private TheTableController control = new TheTableController(model);
    private DefaultTableModel dataModel;
    private JTable table;
    JMenuBar jmb = new JMenuBar();
    JMenu fileMenu = new JMenu("File");
    JMenu queryMenu = new JMenu("Query");
    JMenuItem jminew = new JMenuItem("New");
    JMenuItem jmiopen = new JMenuItem("Open");
    JMenuItem jmiclose = new JMenuItem("Close");
    JMenu jmiexport = new JMenu("Export");
    JMenuItem jmiexit = new JMenuItem("Exit");
    JMenuItem jmiCSV = new JMenuItem("CSV");
    JMenuItem jmiHTML = new JMenuItem("HTML");
    JMenuItem jmiSave = new JMenuItem("Save");
    JMenuItem jmiExecute = new JMenuItem("Execute");
    JMenuItem jmiOpenQuery = new JMenuItem("Open");
    JPanel filePanel = new JPanel();
    JFileChooser jSQLiteChooser = new JFileChooser(new File("."));
    final JEditorPane codeEditor = new JEditorPane();
    JScrollPane jspn1 = new JScrollPane();
    JScrollPane jspn2 = new JScrollPane();
    JButton jbtExecuteSQL = new JButton("Execute SQL Command");
    JButton jbtClearSQLCommand = new JButton("Clear SQL");
    JButton jbtClearResult = new JButton("Clear result");
    JButton jbtSaveQuery = new JButton("Save");
    JButton jbtExecuteAll = new JButton("Execute All");
    JButton jbtRemove = new JButton("Remove");
    Border titleBorder1 = new TitledBorder("Enter an SQL Command");
    Border titleBorder2 = new TitledBorder("SQL Execution Result");
    JLabel jlbConnectionStatus = new JLabel("No connection now");
    JLabel jlbSQLStatus = new JLabel();
    JList jlsTableName = new JList();
    JList jlsColumnName = new JList();
    DefaultListModel tableName = new DefaultListModel();
    TableJList tableList = new TableJList(model);
    QueryJList queryList = new QueryJList(model);
    boolean checkOpen = false;

    public TheTableView(TheTableModel model) {

        this.model = model;
        //add the observer which is the table list and query list
        model.addObserver(tableList);
        model.addObserver(queryList);

        //set the menu bar to the frame
        this.setJMenuBar(jmb);
        jmb.add(fileMenu);
        jmb.add(queryMenu);

        //add the file chooser
        filePanel.add(jSQLiteChooser);

        //add the menu item to the menu
        jmiexport.add(jmiCSV);
        jmiexport.add(jmiHTML);
        fileMenu.add(jminew);
        fileMenu.add(jmiopen);
        fileMenu.add(jmiclose);
        fileMenu.addSeparator();
        fileMenu.add(jmiexport);
        fileMenu.addSeparator();
        fileMenu.add(jmiexit);

        queryMenu.add(jmiOpenQuery);
        queryMenu.add(jmiSave);
        queryMenu.add(jmiExecute);

        //add listener to all the relevant component
        jminew.addActionListener(this);
        jmiopen.addActionListener(this);
        jmiclose.addActionListener(this);
        jmiexit.addActionListener(this);
        jmiCSV.addActionListener(this);
        jmiHTML.addActionListener(this);
        jmiSave.addActionListener(this);
        jmiOpenQuery.addActionListener(this);
        jmiExecute.addActionListener(this);
        jbtSaveQuery.addActionListener(this);
        jbtExecuteAll.addActionListener(this);

        DefaultSyntaxKit.initKit();

        //prepare the JSyntax Pane 
        jspn1 = new JScrollPane(codeEditor);
        codeEditor.setPreferredSize(new Dimension(450, 150));
        codeEditor.setContentType("text/sql");
        codeEditor.setText("SELECT \n FROM \n WHERE");
        codeEditor.setFont(new Font("Verdana", Font.ROMAN_BASELINE, 12));
        jspn1.setBorder(titleBorder1);

        //prepare the JTable to display result
        table = new JTable();
        table.setAutoCreateRowSorter(true);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        jspn2 = new JScrollPane(table);
        jspn2.setBorder(titleBorder2);
        jspn2.setViewportView(table);

        //prepare the panel to put in the all button for executed query
        JPanel pn = new JPanel(new FlowLayout());
        pn.add(jbtRemove);
        pn.add(jbtSaveQuery);
        pn.add(jbtExecuteAll);

        //add the clear and execute query to 1 panel
        jbtClearSQLCommand.addActionListener(this);
        JPanel pn1 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        pn1.add(jbtClearSQLCommand);
        pn1.add(jbtExecuteSQL);

        //prepare the connection status
        JPanel pn3 = new JPanel();
        pn3.setLayout(new BorderLayout());
        pn3.add(jlbConnectionStatus, BorderLayout.CENTER);

        //prepare the middle panel, under the JSyntaxPane
        JPanel pn4 = new JPanel();
        pn4.setLayout(new BorderLayout());
        pn4.add(pn1, BorderLayout.WEST);
        pn4.add(pn3, BorderLayout.EAST);

        //add the JList, JSyntaxPane and the middle pane together
        JPanel pn5 = new JPanel();
        pn5.setLayout(new BorderLayout());
        pn5.add(tableList, BorderLayout.WEST);
        pn5.add(jspn1, BorderLayout.CENTER);
        pn5.add(pn4, BorderLayout.SOUTH);

        //add the query JList and the associated button
        JPanel pn7 = new JPanel();
        pn7.setLayout(new BorderLayout());
        pn7.add(queryList, BorderLayout.CENTER);
        pn7.add(pn, BorderLayout.SOUTH);

        //add the JTable, query JList and the sql status together
        jlbSQLStatus.setText("No connection");
        JPanel pn6 = new JPanel();
        pn6.setLayout(new BorderLayout());
        pn6.add(jspn2, BorderLayout.CENTER);
        pn6.add(jlbSQLStatus, BorderLayout.SOUTH);
        pn6.add(pn7, BorderLayout.WEST);

        //add the 2 main panel together
        this.add(pn5, BorderLayout.CENTER);
        this.add(pn6, BorderLayout.SOUTH);

        this.setTitle("SQLite App - Nguyen Viet Cuong");
    }

    //get the SQL command from the JSyntaxPane
    public String getSQL() {
        return codeEditor.getText();
    }

    @Override
    public void update(Observable obj, Object o1) {

        //update the view when the model notify
        if (model.getSQLError() == false) {
            //set the SQL status
            jlbSQLStatus.setText("ERROR.");
        } else {
            this.model = (TheTableModel) obj;
            dataModel = model.getDB().getTableModel();
            try {
                table.setModel(dataModel);
            } catch (Exception e) {
                jlbSQLStatus.setText("ERROR.");
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //the Action listener for all the menu components
        //and those does not change the main data of the model
        if (e.getSource() == jmiopen || e.getSource() == jminew) {
            //create sqlite or open existing file
            jSQLiteChooser.setFileFilter(new SQLiteFilter());
            int returnVal = jSQLiteChooser.showOpenDialog(filePanel);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = jSQLiteChooser.getSelectedFile();
                jlbConnectionStatus.setText("Connected to " + file.getName());
                jlbSQLStatus.setText("Ready");
                model.setDB(file.getName());
                codeEditor.setText("SELECT \nFROM \nWHERE");
            }
        } else if (e.getSource() == jmiexit) {
            //exit the system
            System.exit(0);
        } else if (e.getSource() == jmiCSV) {
            if (model.getDB() != null) {
                //export file to csv format
                jSQLiteChooser.setFileFilter(new CSVFilter());
                int returnVal = jSQLiteChooser.showSaveDialog(filePanel);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = jSQLiteChooser.getSelectedFile();
                    //write the result to csv file
                    writeToCSV(file);
                }
            } else {
                //print error message
                Message error = new Message();
                error.printConnection();
                jlbSQLStatus.setText("Error. No open connection.");
            }
        } else if (e.getSource() == jmiHTML) {
            if (model.getDB() != null) {
                //export file to html format
                jSQLiteChooser.setFileFilter(new HTMLFilter());
                int returnVal = jSQLiteChooser.showSaveDialog(filePanel);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = jSQLiteChooser.getSelectedFile();
                    //write the result to html file
                    writeToHTML(file);
                }
            } else {
                //print error message
                Message error = new Message();
                error.printConnection();
                jlbSQLStatus.setText("Error. No open connection.");
            }
        } else if (e.getSource() == jbtClearSQLCommand) {
            //clear all the sql command
            codeEditor.setText("SELECT \nFROM \nWHERE");
        } else if (e.getSource() == jmiclose) {
            try {
                //close the connection
                model.disconnectDB();
                tableList.clear();
                queryList.clear();
            } catch (Exception ex) {
                //print error message
                Message error = new Message();
                error.printConnection();
                jlbSQLStatus.setText("Error. No open connection.");
            }
        } else if (e.getSource() == jmiSave || e.getSource() == jbtSaveQuery) {
            if (model.getDB() != null) {
                //save the list of query
                jSQLiteChooser.setFileFilter(new SQLFilter());
                int returnVal = jSQLiteChooser.showSaveDialog(filePanel);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = jSQLiteChooser.getSelectedFile();
                    //save query to file text
                    saveQuery(file);
                }
            } else {
                Message error = new Message();
                error.printConnection();
                jlbSQLStatus.setText("Error. No open connection.");
            }
        } else if (e.getSource() == jmiOpenQuery) {
            if (model.getDB() != null) {
                //open the saved query file
                jSQLiteChooser.setFileFilter(new SQLFilter());
                int returnVal = jSQLiteChooser.showOpenDialog(filePanel);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = jSQLiteChooser.getSelectedFile();
                    //open file and read to query JList
                    openQuery(file);
                    checkOpen = true;
                }
            } else {
                Message error = new Message();
                error.printConnection();
                jlbSQLStatus.setText("Error. No open connection.");
            }
        } else if (e.getSource() == jmiExecute || e.getSource() == jbtExecuteAll) {
            //execute all the query in the query JList
            if (model.getDB() != null) {
                jSQLiteChooser.setFileFilter(new CSVFilter());
                int returnVal = jSQLiteChooser.showSaveDialog(filePanel);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = jSQLiteChooser.getSelectedFile();
                    writeResult(file);
                }
            } else {
                Message error = new Message();
                error.printConnection();
            }
        }
    }

    //write result to csv file
    public void writeToCSV(File file) {
        try {
            if (!file.exists()) {
                file.createNewFile();
            }

            //prepare file writer
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            StringBuffer sbTableData = new StringBuffer();

            //get the data from the table
            for (int row = 0; row < table.getRowCount(); row++) {
                sbTableData = new StringBuffer();
                for (int column = 0; column < table.getColumnCount(); column++) {
                    sbTableData.append(table.getValueAt(row, column)).append(",");
                }
                //append data to string buffer
                sbTableData.deleteCharAt(sbTableData.length() - 1);
                //write row to file
                bw.write(sbTableData.toString());
                //new line
                bw.newLine();
            }
            //close the file when done
            bw.close();
        } catch (Exception e) {
            jlbSQLStatus.setText("ERROR.");
        }
    }

    //export to html file
    public void writeToHTML(File file) {
        try {
            if (!file.exists()) {
                file.createNewFile();
            }

            //prepare file writer
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            StringBuffer sbTableData = new StringBuffer();

            //write the html tag to file
            bw.write("<html>");
            bw.write("<head>");
            bw.write("<title>Query Result</title>");
            bw.write("<body>");
            bw.write("<h1>Query Result</h1>");
            bw.write("<table border='1'>");

            //get the data from table
            for (int row = 0; row < table.getRowCount(); row++) {
                //create a row
                bw.write("<tr>");
                bw.newLine();
                sbTableData = new StringBuffer();
                for (int column = 0; column < table.getColumnCount(); column++) {
                    sbTableData.append("<td>");
                    sbTableData.append(table.getValueAt(row, column)).append("</td>");
                    bw.write(sbTableData.toString());
                    sbTableData = new StringBuffer();
                }
                //new line
                bw.newLine();
                bw.write("</tr>");
            }
            bw.newLine();
            bw.write("</table>");
            bw.write("</body>");
            bw.write("</html>");

            //close the file when done
            bw.close();
        } catch (Exception e) {
            jlbSQLStatus.setText("ERROR.");
        }
    }

    //export executed query to file
    public void saveQuery(File file) {
        try {
            if (!file.exists()) {
                file.createNewFile();
            }

            //prepare file writer
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            StringBuffer sbTableData = new StringBuffer();

            //prepare string array to store query
            String[] s = new String[1000];
            //take query from ArrayList to string array
            model.getQuery().toArray(s);

            //write the query to file
            for (int i = 0; i < s.length; i++) {
                if (s[i] == null || s[i] == "") {
                    break;
                } else {
                    sbTableData = new StringBuffer();
                    //end each query with semi-colon
                    sbTableData.append(s[i]).append(";");
                    bw.write(sbTableData.toString());
                    bw.newLine();
                }
            }
            bw.close();
        } catch (Exception e) {
            jlbSQLStatus.setText("ERROR.");
        }
    }

    //open the save query file
    public void openQuery(File file) {
        try {

            //prepare file reader
            BufferedReader br = new BufferedReader(new FileReader(file.getName()));

            //reset the query
            model.setQuery();
            //store value of each line to string
            String sCurrentLine;
            StringBuffer s = new StringBuffer();

            //change the query ArrayList
            while ((sCurrentLine = br.readLine()) != null) {
                s.append(sCurrentLine);
                if (sCurrentLine.trim().endsWith(";")) {
                    //each query end with ;
                    //add query to the ArrayList
                    model.addQuery(s.toString());
                    s = new StringBuffer();
                }
            }
            model.viewQuery();

        } catch (Exception e) {
            Message error = new Message();
            error.printConnection();
            jlbSQLStatus.setText("ERROR.");
        }
    }

    //write the result of execute all to file
    public void writeResult(File file) {
        try {
            if (!file.exists()) {
                file.createNewFile();
            }

            //prepare file writer
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            StringBuffer sbTableData = new StringBuffer();

            //for loop to run all the query
            for (int i = 0; i < model.getQuery().size(); i++) {
                sbTableData = new StringBuffer();
                //execute the query of index i in the ArrayList
                model.executeQuery(i);
                //set the default model
                table.setModel(dataModel);
                //write the query to file
                sbTableData.append(model.getQuery().get(i));
                bw.write(sbTableData.toString());
                bw.newLine();
                bw.newLine();
                //write the result of the query to file
                for (int row = 0; row < table.getRowCount(); row++) {
                    sbTableData = new StringBuffer();
                    for (int column = 0; column < table.getColumnCount(); column++) {
                        sbTableData.append(table.getValueAt(row, column)).append(",");
                    }
                    sbTableData.deleteCharAt(sbTableData.length() - 1);
                    bw.write(sbTableData.toString());
                    bw.newLine();
                }
                bw.newLine();
            }
            //close file when finish
            bw.close();
            table.removeAll();
        } catch (Exception e) {
            jlbSQLStatus.setText("ERROR.");
        }
    }

    //sqlite file filter
    class SQLiteFilter extends FileFilter {

        @Override
        public boolean accept(File f) {
            return f.isDirectory() || f.getName().toLowerCase().endsWith(".sqlite");
        }

        @Override
        public String getDescription() {
            return ".sqlite";
        }
    }

    //csv file filter
    class CSVFilter extends FileFilter {

        @Override
        public boolean accept(File f) {
            return f.isDirectory() || f.getName().toLowerCase().endsWith(".txt") || f.getName().toLowerCase().endsWith(".csv");
        }

        @Override
        public String getDescription() {
            return ".txt/csv";
        }
    }

    //html file filter
    class HTMLFilter extends FileFilter {

        @Override
        public boolean accept(File f) {
            return f.isDirectory() || f.getName().toLowerCase().endsWith(".html");
        }

        @Override
        public String getDescription() {
            return ".html";
        }
    }

    //.sql file filter
    class SQLFilter extends FileFilter {

        @Override
        public boolean accept(File f) {
            return f.isDirectory() || f.getName().toLowerCase().endsWith(".sql");
        }

        @Override
        public String getDescription() {
            return ".sql";
        }
    }
}
