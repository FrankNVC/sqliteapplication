package Part2_SQLiteApp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Nguyen Viet Cuong - s3393346
 */
public class QueryJList extends JPanel implements Observer {

    //create a JList
    private JList queryList = new JList();
    //List model to add element to JList
    private DefaultListModel queryListModel = new DefaultListModel();
    //connect to the model
    private TheTableModel model;
    //scroll pane to put the JList on
    private JScrollPane jspnQueryPane = new JScrollPane(queryList);

    //constructor
    public QueryJList(TheTableModel model) {
        //set model
        this.model = model;
        //set layout
        this.setLayout(new BorderLayout());

        //connect JList with the list model
        queryList = new JList(queryListModel);
        //put the JList on the scroll pane
        jspnQueryPane = new JScrollPane(queryList);
        jspnQueryPane.setPreferredSize(new Dimension(300, 450));

        //add the scroll pane to the main panel
        this.add(jspnQueryPane, BorderLayout.CENTER);
    }

    @Override
    public void update(Observable o, Object arg) {
        //when the query is added
        //clear the JList
        //re-adding the query using updateView
        model = (TheTableModel) arg;
        queryListModel.removeAllElements();
        updateView();
    }

    //update the view
    public void updateView() {
        try {
            //get dataModel
            DefaultTableModel dataModel = model.getDB().getTableModel();
            String[] s = new String[dataModel.getRowCount()];
            //write the arrayList executedQuery to string array s
            model.getQuery().toArray(s);
            for (int i = 0; i < s.length; i++) {
                //add the query to the JList
                queryListModel.addElement(s[i]);
            }
        } catch (Exception e) {
        }
    }

    public JList getQueryJList() {
        //accessor JList
        return queryList;
    }

    public DefaultListModel getListModel() {
        //accessor ListModel
        return queryListModel;
    }

    public void removeQuery(int index) {
        //remove the selected query on the JList
        queryListModel.removeElementAt(index);
    }

    //clear all the element of the JList
    public void clear() {
        queryListModel.removeAllElements();
    }
}
