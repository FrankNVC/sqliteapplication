package Part2_SQLiteApp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Nguyen Viet Cuong - s3393346
 */
public class TableJList extends JPanel implements Observer {

    //create JList
    private JList tableList = new JList();
    private JList columnList = new JList();
    //list model to add element to JList
    private DefaultListModel tableListModel = new DefaultListModel();
    private DefaultListModel columnListModel = new DefaultListModel();
    //connect to model
    private TheTableModel model;
    //scroll pane to put the JList on
    private JScrollPane tablePane = new JScrollPane(tableList);
    private JScrollPane columnPane = new JScrollPane(columnList);

    //constructor
    public TableJList(TheTableModel model) {
        //set model
        this.model = model;
        //set layout
        this.setLayout(new BorderLayout());

        //prepare the JList for table name
        tableList = new JList(tableListModel);
        tablePane = new JScrollPane(tableList);
        tablePane.setPreferredSize(new Dimension(200, 150));

        //prepare the JList for column name
        columnList = new JList(columnListModel);
        columnPane = new JScrollPane(columnList);
        columnPane.setPreferredSize(new Dimension(150, 150));

        //add the scroll pane to the main panel
        this.add(tablePane, BorderLayout.CENTER);
        this.add(columnPane, BorderLayout.EAST);
    }

    @Override
    public void update(Observable o, Object arg) {
        //whenever a query is executed
        //clear the JList
        //re-adding the element using updateView
        model = (TheTableModel) arg;
        tableListModel.removeAllElements();
        columnListModel.removeAllElements();
        updateView();
    }

    //update the view
    public void updateView() {
        try {
            DefaultTableModel dataModel = model.getDB().getTableModel();
            String[] s = new String[dataModel.getRowCount()];
            s = model.getTableName().getString();
            for (int i = 0; i < s.length; i++) {
                tableListModel.addElement(s[i]);
            }
            s = model.getColumn().getString();
            for (int i = 0; i < s.length; i++) {
                columnListModel.addElement(s[i]);
            }
        } catch (Exception e) {
        }
    }

    //accessor
    public JList getTableJList() {
        return tableList;
    }

    //accessor
    public JList getComlumnJList() {
        return columnList;
    }

    //clear all the element of the JList
    public void clear() {
        tableListModel.removeAllElements();
        columnListModel.removeAllElements();
    }
}